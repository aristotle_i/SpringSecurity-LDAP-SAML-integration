package com.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author ankidaemon
 *
 */
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.demo.config")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		//using local embeded LDAP server takes localhost.
		auth
		.ldapAuthentication()
		.contextSource()
				//need to specify the root because there is no URL
			.root("dc=packt,dc=com")
				//also need to define the file since it's embeded
			.ldif("classpath:packt.ldif")
		.and()
				//search the incomming users we are using the first pattern because both users are in finance.
			.userDnPatterns("uid={0},ou=finance")
				//specify where your group searching should start, in our case groups
			.groupSearchBase("ou=groups");
	}

	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	///http config  if chief from the user should have the admin authority
		http.authorizeRequests()
		.regexMatchers("/chief/.*").hasRole("ADMIN")
				//accesing agent has the authority user
		.regexMatchers("/agent/.*").access("hasRole('USER')")
		.anyRequest()
				.authenticated()
				.and().httpBasic()	
				.and().requiresChannel().anyRequest().requiresSecure();

		http.formLogin().loginPage("/login").permitAll();
		http.logout().logoutSuccessUrl("/");
		http.exceptionHandling().accessDeniedPage("/accessDenied");
		
	}

}

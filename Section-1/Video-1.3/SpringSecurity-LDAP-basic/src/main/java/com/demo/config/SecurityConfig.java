package com.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author ankidaemon
 *
 * The spring security code
 */
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.demo.config")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	//describes the type of authenticaiton that we need in this case it's LDAP
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth
		.ldapAuthentication()
		.contextSource()
				//the source is the server, we have the port and the domain component 'packt.com'
		.url("ldap://packt.com:389/dc=packt,dc=com")
				//for tls security we have the following
		//.url("ldaps://packt.com:636/dc=packt,dc=com")  //For TLS
		//the lines below has the crediantialze
		.managerDn("managerDN")
		.managerPassword("managerPassword")
				//bind authentiation ldap userDN = user id {0} parameter passed to it for example the person that is logging in
		.and()
			.userDnPatterns("uid={0},ou=finance")
				//loads the group or the authorities that the user has
			.groupSearchBase("ou=groups");
	    //search users  with filters this is user searched based ldap
		auth.ldapAuthentication()
			.userSearchFilter("{uid={0}}")
			.userSearchBase("ou=finance").groupSearchBase("ou=groups");
	}

	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
		.anyRequest()
				.authenticated()
				.and().httpBasic()	
				.and().requiresChannel().anyRequest().requiresSecure();

		http.formLogin().loginPage("/login").permitAll();
		http.logout().logoutSuccessUrl("/");
		
	}

}
